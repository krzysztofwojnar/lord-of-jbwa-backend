import { Field } from "./Field";
import { IActionStrategy } from "./FieldActions/IActionStrategy";
import { NonInitializedBoard } from "./NonInitializedBoard";

export class Board {
    constructor(board: NonInitializedBoard){
        
        this.fields = board.getFields().map(nonInitializedField => new Field(nonInitializedField))
    }
    private fields: Field<IActionStrategy>[];
}