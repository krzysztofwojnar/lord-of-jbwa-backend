﻿import { Player } from './Player';
import { NonInitializedField } from './NonInitializedField';
import { IActionStrategy } from './FieldActions/IActionStrategy';

export class Field <T extends IActionStrategy> {
  public players: Player[] = [];

  constructor(nonInitalizedField: NonInitializedField<T>) {
    this.label = nonInitalizedField.getLabel();
    this.fullName = nonInitalizedField.getFullName();
    this.display = nonInitalizedField.getDisplay();
    this.action = nonInitalizedField.getAction();
    const newPosition = nonInitalizedField.getPosition();
    this.position = newPosition ? newPosition : -1;
  }
  protected readonly label: string;
  protected readonly fullName: string;
  public display: { headerColor?: string, imageSrc?: string };
  private readonly position: number;
  protected action: T;

  getLabel(): string {
    return this.label;
  }
  getFullName(): string {
    return this.fullName;
  }
  getPosition(): number {
    return this.position;
  }
  getAction(): T {
    return this.action;
  }
  getDisplay(): { headerColor?: string, imageSrc?: string } {
    return this.display;
  }

}
