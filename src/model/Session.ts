import { Player } from './Player';
import { Game } from './Game';
import { Lobby } from './Lobby';

export class Session {
    constructor(id: number, owner: Player) {
        this.lobby = new Lobby(owner);
        this.id = id;
    }
    private lobby: Lobby;
    public getLobby(): Lobby {
        return this.lobby;
    }
    public getOrStartGame(): Game {
        if (this.game) return this.game;
        this.game = new Game(this.lobby);
        return this.game;
    }
    private game?: Game;
    private id: number;
    public getID(): number {
        return this.id;
    };
}