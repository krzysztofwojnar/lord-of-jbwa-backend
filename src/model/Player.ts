﻿export class Player{
    constructor(nickname: string){
        this.nickname = nickname;
    }
    private readonly nickname: string;
    private balance = 0;
    private turnsToGoOut = 0;
    private position = 0;
    public setPosition (newPosition: number) {
        this.position = newPosition;
    }
    public getPosition () {
        return this.position;
    }
    public getNickname () {
        return this.nickname;
    }
    public updateBalance(amount: number) {
        this.balance += amount;
    }
    public ban(turns: number) {
        this.turnsToGoOut += turns;
    }
    public getBalance(): number {
        return this.balance;
    }
    public debit(toPay: number, recipient?: Player) {
        if (this.getBalance() < toPay) {
            this.updateBalance(-toPay);
            console.error('Asking for selling properties not implemented')
        }
        this.updateBalance(-toPay);
        if (recipient) recipient.updateBalance(toPay);
        
    }


}
