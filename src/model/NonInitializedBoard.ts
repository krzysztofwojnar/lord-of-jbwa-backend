import { FieldsService } from "../fieldsService";
import { IActionStrategy } from "./FieldActions/IActionStrategy";
import { NonInitializedField } from "./NonInitializedField";

export class NonInitializedBoard {
    constructor(){
        this.fields = FieldsService.fieldsService.getNonInitializedFields();
    }
    private readonly fields: NonInitializedField<IActionStrategy>[];
    public getFields(): NonInitializedField<IActionStrategy>[] {
        return this.fields;
    }
}
