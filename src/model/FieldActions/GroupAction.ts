import { Player } from "../Player";
import { ClaimableFieldAction } from "./ClaimableFieldAction";

export class GroupAction extends ClaimableFieldAction {
    constructor(cost: number, payments:number[], moderatorsCost: number) {
        super(cost);
        this.moderatorsCost = moderatorsCost;
        this.payments = payments;
    }
    private moderators: number = 0;
    private readonly moderatorsCost: number;
    private readonly payments: number[];
    protected removeOwner(): void {
        super.removeOwner();
        this.moderators = 0;
    }

    public getModerators(): number {
        return this.moderators;
    }
    public addModerator(): boolean {
        if (!this.owner) {
            console.error('Owner does not exists!');
            return false;
        }
        if (this.moderatorsCost <= this.owner?.getBalance()) {
            this.owner?.updateBalance(-this.moderatorsCost);
            this.moderators++;
            return true;
        } else return false;
    }
    public removeModerator(): boolean {
        if (!this.owner) {
            console.error('Owner does not exists!');
            return false;
        }
        if (0 < this.moderators) {
            this.owner.updateBalance(this.moderatorsCost / 2);
            this.moderators--;
            return true;
        } else return false;

    }

    protected payToOwner(activePlayer: Player) {
        throw new Error('Paying to owner is not implemented.');
        return;
    }

}