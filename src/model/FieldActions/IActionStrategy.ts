import { Field } from "../Field";
import { Player } from "../Player";

export interface IActionStrategy {
    action: (activePlayer: Player, owner?: Player, field?: Field<any>) => void;
}