import { Field } from "../Field";
import { Player } from "../Player";
import { IActionStrategy } from "./IActionStrategy";

export abstract class ClaimableFieldAction implements IActionStrategy {
    constructor(cost: number) {
        this.cost = cost;

    }
    protected owner?: Player;
    protected removeOwner() { this.owner = undefined }
    protected readonly cost: number;
    public getOwner(): Player | undefined {
        return this.owner;
    }


    static async askForBuy(activePlayer: Player, cost: number): Promise<boolean> {
        console.error('asking for buying properties is not implemented');
        if (activePlayer.getBalance() < cost) return new Promise<boolean>(resolve => resolve(false));
        return new Promise(resolve => resolve(true));

    }
    async askForPurchase(activePlayer: Player) {
        if (await ClaimableFieldAction.askForBuy(activePlayer, this.cost)) {
            this.owner = activePlayer;
        }
    }
    action(activePlayer: Player, owner?: Player) {
        if (owner) {
            this.payToOwner(activePlayer)
        } else {
            if (!this.askForPurchase(activePlayer)) this.auction(activePlayer);
        }

    }
    auction(activePlayer: Player): Player {
        throw (new Error('auction not implemented'));
    }

    protected abstract payToOwner(activePlayer: Player): void;
}


