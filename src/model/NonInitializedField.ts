import { IActionStrategy } from "./FieldActions/IActionStrategy";

export class NonInitializedField <T extends IActionStrategy> {
    constructor(
        protected label: string,
        protected fullName: string,
        protected action: T,
        protected display: { headerColor?: string, imgSrc?: string },
        protected position?: number
    ) {
    }
    getLabel(): string { return this.label; }
    setLabel(newLabel: string): void { this.label = newLabel; }
    getFullName(): string { return this.fullName; }
    setFullName(newFullName: string): void { this.fullName = newFullName; }
    getPosition(): number | undefined { return this.position; }
    setPosition(newPosition: number): void {
        if (newPosition >= 0) {
            this.position = newPosition;
        } else { throw new Error('New position is incorrect! ( ' + this.position + ' -> ' + newPosition + ')'); }
    }
    getDisplay(): { headerColor?: string, imgSrc?: string } { return this.display; }
    setDisplay(newDisplay: { headerColor?: string, imgSrc?: string }): void { this.display = newDisplay };
    getAction(): T { return this.action; }
    setAction(newAction: T) { this.action = newAction }
}