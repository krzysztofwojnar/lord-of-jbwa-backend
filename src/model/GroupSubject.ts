import { Field } from "./Field";
import { IActionStrategy } from "./FieldActions/IActionStrategy";
import { NonInitializedField } from "./NonInitializedField";

export class GroupSubject<T extends IActionStrategy> {
    private sameSubjectGroups: Set<Field<T> | NonInitializedField<T>> = new Set();
    public addGroupWithSameSubject(group: Field<T> | NonInitializedField<T>): Field<T> | NonInitializedField<T> {
        this.sameSubjectGroups.add(group);
        return group;
    }
    public getGroupsWithSameSubject(): Set<Field<T> | NonInitializedField<T>> {
        return new Set(this.sameSubjectGroups);
    }
}