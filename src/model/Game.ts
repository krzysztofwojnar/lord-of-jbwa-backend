import { Lobby } from "./Lobby";
import { Board } from "./Board";
import { Player } from "./Player";

export class Game {
    constructor(lobby: Lobby){
        this.activePlayers = lobby.getPlayers();
        this.board = new Board(lobby.getBoard());
    }
    private activePlayers: Player[];
    private losersPlayers: Player[] = [];
    private board: Board;

    public getPlayers(): Player[] {
        return this.activePlayers.concat(this.losersPlayers);
    }
    public losePlayers(player: Player): void {
        const playerIndex = this.activePlayers.indexOf(player)
        if(0 <= this.activePlayers.indexOf(player)){
            this.activePlayers = this.activePlayers.slice(0,playerIndex).concat(this.activePlayers.slice(playerIndex+1));
            this.losersPlayers.push(player);
        }
        
    }
}