import { NonInitializedBoard } from "./NonInitializedBoard";
import { Player } from "./Player";

export class Lobby {
    constructor(host: Player) {
        this.host = host;
        this.players = [this.host];
    }
    private host: Player;
    private board = new NonInitializedBoard();
    private players: Player[];
    public getBoard(): NonInitializedBoard {
        return this.board;
    }
    public getPlayers(): Player[] {
        return this.players;
    }
}