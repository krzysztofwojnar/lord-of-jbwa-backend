﻿import {Field} from './model/Field';
import {Player} from './model/Player';
import {defaultGroupsNames} from './data/defaultGroupsNames';
import {NonInitializedField} from './model/NonInitializedField';
import {ClaimableFieldAction} from './model/FieldActions/ClaimableFieldAction';
import {GroupSubject} from './model/GroupSubject';
import {GroupAction} from './model/FieldActions/GroupAction';
import {getCosts} from './data/groupCostsAndPayments';
import {diceRolls} from './app';
import {IActionStrategy} from './model/FieldActions/IActionStrategy';

class Chan extends ClaimableFieldAction {
    constructor(cost: number) {
        super(cost)
    }

    payments = [50, 100, 200, 400]

    payToOwner(activePlayer: Player) {
        const sameOwnerChans = Array.from(FieldsService.allSpecials.getGroupsWithSameSubject()).filter(field => field.getAction().getOwner() === this.owner).length;
        const payment = this.payments[sameOwnerChans - 1];
        activePlayer.debit(payment, this.getOwner());
    }
}

class SpecialAction extends ClaimableFieldAction {
    constructor(cost: number) {
        super(cost)
    }

    payToOwner(activePlayer: Player) {
        const roll = diceRolls()
        const multiplier = Array.from(FieldsService.allSpecials.getGroupsWithSameSubject()).filter(field => field.getAction().getOwner() === this.owner).length;
        const payment = multiplier * (roll.firstDice + roll.secondDice);
        activePlayer.debit(payment, this.getOwner());
    }
}

export class FieldsService {
    public static readonly fieldsService: FieldsService = new FieldsService();

    public static readonly getDefaultGroups: Array<NonInitializedField<GroupAction>> = (() => {
        const getNextGroupCost = FieldsService.nextGroupCost();
        return defaultGroupsNames.map((groupName: { label: string, fullName: string }, index: number) => {
                // console.log(getNextGroupCost());
                const displaySettings = getNextGroupCost();
                return new NonInitializedField(
                    groupName.label,
                    groupName.fullName,
                    new GroupAction(displaySettings.cost, displaySettings.payments, displaySettings.moderatorsCost),

                    {headerColor: displaySettings.header},
                );
            }
        ).reverse();
    })();
    public static readonly groups: Array<NonInitializedField<GroupAction>> = (() => {
        const getNextGroupCost = FieldsService.nextGroupCost();
        return defaultGroupsNames.map((groupName: { label: string, fullName: string }) => {
                const displaySettings = getNextGroupCost();
                return new NonInitializedField(
                    groupName.label,
                    groupName.fullName,
                    new GroupAction(
                        displaySettings.cost,
                        displaySettings.payments,
                        displaySettings.moderatorsCost,
                    ),
                    {headerColor: displaySettings.header},
                    undefined
                );
            }
        ).reverse();
    })();
    public static allChans: GroupSubject<Chan> = new GroupSubject<Chan>();

    /*public static Chan = class extends ClaimableFieldAction {
      constructor(cost: number) {
        super(cost)
      }
      payments = [50, 100, 200, 400]
      payToOwner(activePlayer: Player) {
        const sameOwnerChans = Array.from(FieldsService.allSpecials.getGroupsWithSameSubject()).filter(field => field.getAction().getOwner() === this.owner).length;
        const payment = this.payments[sameOwnerChans-1];
        activePlayer.debit(payment, this.getOwner());
      }
    }*/

    public readonly chans: NonInitializedField<(Chan)>[] = ['4chan', 'wykop', 'vichan', 'karachan']
        .map((name) => {
            const nonInitializedChan = new NonInitializedField(
                name,
                name,
                new Chan(400),
                {},
                undefined
            );
            if (!FieldsService.allChans) FieldsService.allChans = new GroupSubject<Chan>()
            FieldsService.allChans.addGroupWithSameSubject(nonInitializedChan);
            return nonInitializedChan;
        });

    public static next(): () => NonInitializedField<GroupAction> {
        let currentIndex = 0;
        return () => FieldsService.groups[currentIndex++];
    }

    public static nextGroupCost(): () => { cost: number, payments: number[], moderatorsCost: number, header: string } {
        let currentIndex = 0;
        const costs = getCosts().reverse().flatMap(x => x);
        return () => costs[currentIndex++];
    }

    chanAction = {
        action: (field: Field<ClaimableFieldAction>, activePlayer: Player) => {
            throw new Error('Chan actions are not implemented!');
        }
    }


    start = new NonInitializedField<IActionStrategy>('start', 'Start', {
        action: () => {
            return;
        }
    }, {});
    warn1 = new NonInitializedField<IActionStrategy>('warn', 'Twój post nie spełnia standardów społeczności', {
            action: (activePlayer: Player) => {
                activePlayer.updateBalance(-400);
            }
        }, {}
    );
    private static blueChance = {
        action: (activePlayer: Player) => {
            throw new Error('Blue Chance is not implemented!');
        }
    }

    private static redChance = {
        action: (activePlayer: Player) => {
            throw new Error('Red Chance is not implemented!');
        }
    }

    public static chanceFactory(chanceType: 'red' | 'blue'): NonInitializedField<IActionStrategy> {
        switch (chanceType) {
            case 'blue':
                return new NonInitializedField<IActionStrategy>('cenzopapa', 'Cenzopapa', this.blueChance, {imgSrc: 'assets/jp2.png'});
            case 'red':
                return new NonInitializedField<IActionStrategy>('testo', 'Wielki Testo', this.redChance, {imgSrc: 'assets/testo.jpg'});
        }
    }

    aresztawka = new NonInitializedField<IActionStrategy>('aresztawka', 'Weryfikacja', {
        action: () => {
            return;
        }
    }, {});
    private static readonly specialFieldCost: number = 300;
    public static allSpecials: GroupSubject<SpecialAction> = new GroupSubject<SpecialAction>();

    hajsownicy = (() => {
        const field = new NonInitializedField<SpecialAction>(
            'hajsownicy',
            'Hajsownicy Gimpera',
            new SpecialAction(FieldsService.specialFieldCost),
            {}
        );
        if (!FieldsService.allSpecials) FieldsService.allSpecials = new GroupSubject<SpecialAction>();
        FieldsService.allSpecials.addGroupWithSameSubject(field);
        return field;
    })();

    shitpost = new NonInitializedField('shitpost', 'Shitpost', {
        action: () => {
        }
    }, {imgSrc: 'assets/shitpost.jpg'});
    bp = (() => {
        const field = new NonInitializedField<SpecialAction>(
            'bp',
            'Bękarty Putina',
            new SpecialAction(FieldsService.specialFieldCost),
            {}
        );
        if (!FieldsService.allSpecials) FieldsService.allSpecials = new GroupSubject<SpecialAction>();
        FieldsService.allSpecials.addGroupWithSameSubject(field);
        return field;
    })();

    ban = new NonInitializedField(
        'ban',
        'You are temporarily blocked',
        {
            action: (activePlayer: Player) => {
                activePlayer.ban(3);
            }
        },
        {}
    );
    warn2 = new NonInitializedField(
        'warn',
        'Twój komentarz nie spełnia standardów społeczności',
        {
            action: (activePlayer: Player) => {
                activePlayer.updateBalance(-200);
            }
        },
        {}
    );

    getNonInitializedFields(): NonInitializedField<IActionStrategy>[] {
        const nextGroup = FieldsService.next();
        const blue = 'blue';
        const red = 'red'
        const allGroups = [
            this.start,
            nextGroup(),
            FieldsService.chanceFactory(blue),
            //Object.create(this.cenzopapa),
            nextGroup(),
            this.warn1,
            this.chans[0],
            nextGroup(),
            FieldsService.chanceFactory(red),
            nextGroup(),
            nextGroup(),

            this.aresztawka,
            nextGroup(),
            this.hajsownicy,
            nextGroup(),
            nextGroup(),
            this.chans[1],
            nextGroup(),
            FieldsService.chanceFactory(blue),
            nextGroup(),
            nextGroup(),

            this.shitpost,
            nextGroup(),
            FieldsService.chanceFactory(red),
            nextGroup(),
            nextGroup(),
            this.chans[2],
            nextGroup(),
            nextGroup(),
            this.bp,
            nextGroup(),

            this.ban,
            nextGroup(),
            nextGroup(),
            FieldsService.chanceFactory(blue),
            nextGroup(),
            this.chans[3],
            FieldsService.chanceFactory(red),
            nextGroup(),
            this.warn2,
            nextGroup(),
        ];
        allGroups.forEach((elem, index) => elem.setPosition(index));
        return allGroups;
    }
}



