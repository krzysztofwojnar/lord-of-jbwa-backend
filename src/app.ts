﻿import express, { Application, Request, Response, NextFunction } from 'express';
import { Session } from './model/Session';
import { Player } from './model/Player';
import * as http from 'http';
import * as WebSocket from 'ws';
const enum RequestTypes {
    DICE_ROLL = 'dice-roll',
    GAME_INIT = 'game-init',
    START_GAME = 'start-game',
    UPDATE_PLAYERS = 'update-players',
    UPDATE_BOARD = 'update-board',
    MAKE_AN_OFFER = 'make-an-offer',
    GIVE_UP = 'give-up'
}
console.log('hello world!');
const app: Application = express();

//simple http server
const server = http.createServer(app);

//WebSocket server instance
const webSocketServer = new WebSocket.Server({ server });

const games: Session[] = [];
let counter = 0;
const singleDiceRoll: ( () => number ) = () => Math.floor((Math.random()*6)) + 1;
export const diceRolls: () => { firstDice: number, secondDice: number } = () => {
    return {firstDice: singleDiceRoll(), secondDice: singleDiceRoll()};
}

const aliveConnections: Set<WebSocket> = new Set();

webSocketServer.on('connection', (ws: WebSocket) => {
    aliveConnections.add(ws);

    ws.on('pong', ()=> {
        aliveConnections.add(ws);
        ws.send('You are alive;');
    } );
    ws.on('message', (messageArrayBuffer) => {
        const message: string = messageArrayBuffer.toString();
        console.log(message);
        const NOT_IMPLEMENTED = 'not implemented yet!';
        if (message.match(RequestTypes.MAKE_AN_OFFER + '-.*')) {
            console.log('I see, you want to make a deal with user ' + message.substring(RequestTypes.MAKE_AN_OFFER.length+1));
            ws.send('I see, you want to make a deal with user ' + message.substring(RequestTypes.MAKE_AN_OFFER.length+1));
        } else switch(message) {
            case RequestTypes.DICE_ROLL:
                const roll = JSON.stringify({requestType: message, results: diceRolls(), drawNumber: ++counter});
                console.log(roll);
                ws.send(roll);
                break;
            case RequestTypes.START_GAME:
                const startedGame = new Session(0, new Player('BlaskZKibla'));
                const gamePosition = games.push(startedGame);
                //console.log(JSON.stringify({requestType: message, ...startedGame.getLobby().getBoard()}));
                ws.send(JSON.stringify({requestType: message, ...startedGame.getLobby().getBoard()}));
                break;
            case RequestTypes.UPDATE_PLAYERS:
                ws.send(message + ' is ' + NOT_IMPLEMENTED);
                throw new Error(NOT_IMPLEMENTED);
                break;
            case RequestTypes.UPDATE_BOARD:
                ws.send(message + ' is ' + NOT_IMPLEMENTED);
                throw new Error(NOT_IMPLEMENTED);
                break;
            default:
                ws.send(message + ' not recognized');
                //ws.close(404);


        }
    });
});

/*
setInterval(() => {

    console.log(webSocketServer.clients.size);
    // @ts-ignore
    webSocketServer.clients.forEach((ws: WebSocket) => {
        // console.log(aliveConnections.has(ws));
        if (!aliveConnections.has(ws)) ws.terminate();
        ws.ping(null, false);
    } );
    aliveConnections.clear();
}, 10000);
*/

//start our server
server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on port ${ server.address() } :)`);
    console.log(server.address());
});




/*
const API_URL = '*';
const options:cors.CorsOptions = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: API_URL,
    preflightContinue: false
  };
  
app.use(cors(options));

app.get('/dice-roll', (req: Request, res: Response, next: NextFunction) => {
    res.send(
        function(){
            let roll = JSON.stringify({results: diceRolls(), drawNumber: ++counter});
            console.log(roll);
            return roll;
    }());
});
app.get('/start-game', (req: Request, res: Response, next: NextFunction) => {
    res.setHeader('content-type', 'application/json');
    console.log(req)
    res.send((()=>{
        const startedGame = new Session(0, new Player('BlaskZKibla'));
        const gamePosition = games.push(startedGame)
        return JSON.stringify(startedGame.getLobby().getBoard());
    })())
})
app.get('/update-players', (req: Request, res: Response, next: NextFunction) => {
    res.setHeader('content-type', 'application/json');
    console.log(req);
    console.log('elo');
    res.send((()=>{
        const startedGame = games[games.length-1];
        const positions = startedGame.getLobby().getPlayers().map(
            player => ({nickname: player.getNickname(), position: player.getPosition()})
        )
        return JSON.stringify(positions);
    })())
})

app.listen(5000, () => { console.log('Server running') });

 */
