"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Player = void 0;
class Player {
    constructor(nickname) {
        this.balance = 0;
        this.turnsToGoOut = 0;
        this.position = 0;
        this.nickname = nickname;
    }
    setPosition(newPosition) {
        this.position = newPosition;
    }
    getPosition() {
        return this.position;
    }
    getNickname() {
        return this.nickname;
    }
    updateBalance(amount) {
        this.balance += amount;
    }
    ban(turns) {
        this.turnsToGoOut += turns;
    }
    getBalance() {
        return this.balance;
    }
    debit(toPay, recipient) {
        if (this.getBalance() < toPay) {
            this.updateBalance(-toPay);
            console.error('Asking for selling properties not implemented');
        }
        this.updateBalance(-toPay);
        if (recipient)
            recipient.updateBalance(toPay);
    }
}
exports.Player = Player;
