"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Field = void 0;
class Field {
    constructor(nonInitalizedField) {
        this.players = [];
        this.label = nonInitalizedField.getLabel();
        this.fullName = nonInitalizedField.getFullName();
        this.display = nonInitalizedField.getDisplay();
        this.action = nonInitalizedField.getAction();
        const newPosition = nonInitalizedField.getPosition();
        this.position = newPosition ? newPosition : -1;
    }
    getLabel() {
        return this.label;
    }
    getFullName() {
        return this.fullName;
    }
    getPosition() {
        return this.position;
    }
    getAction() {
        return this.action;
    }
    getDisplay() {
        return this.display;
    }
}
exports.Field = Field;
