"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClaimableFieldAction = void 0;
class ClaimableFieldAction {
    constructor(cost) {
        this.cost = cost;
    }
    removeOwner() { this.owner = undefined; }
    getOwner() {
        return this.owner;
    }
    static async askForBuy(activePlayer, cost) {
        console.error('asking for buying properties is not implemented');
        if (activePlayer.getBalance() < cost)
            return new Promise(resolve => resolve(false));
        return new Promise(resolve => resolve(true));
    }
    async askForPurchase(activePlayer) {
        if (await ClaimableFieldAction.askForBuy(activePlayer, this.cost)) {
            this.owner = activePlayer;
        }
    }
    action(activePlayer, owner) {
        if (owner) {
            this.payToOwner(activePlayer);
        }
        else {
            if (!this.askForPurchase(activePlayer))
                this.auction(activePlayer);
        }
    }
    auction(activePlayer) {
        throw (new Error('auction not implemented'));
    }
}
exports.ClaimableFieldAction = ClaimableFieldAction;
