"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupAction = void 0;
const ClaimableFieldAction_1 = require("./ClaimableFieldAction");
class GroupAction extends ClaimableFieldAction_1.ClaimableFieldAction {
    constructor(cost, payments, moderatorsCost) {
        super(cost);
        this.moderators = 0;
        this.moderatorsCost = moderatorsCost;
        this.payments = payments;
    }
    removeOwner() {
        super.removeOwner();
        this.moderators = 0;
    }
    getModerators() {
        return this.moderators;
    }
    addModerator() {
        var _a, _b;
        if (!this.owner) {
            console.error('Owner does not exists!');
            return false;
        }
        if (this.moderatorsCost <= ((_a = this.owner) === null || _a === void 0 ? void 0 : _a.getBalance())) {
            (_b = this.owner) === null || _b === void 0 ? void 0 : _b.updateBalance(-this.moderatorsCost);
            this.moderators++;
            return true;
        }
        else
            return false;
    }
    removeModerator() {
        if (!this.owner) {
            console.error('Owner does not exists!');
            return false;
        }
        if (0 < this.moderators) {
            this.owner.updateBalance(this.moderatorsCost / 2);
            this.moderators--;
            return true;
        }
        else
            return false;
    }
    payToOwner(activePlayer) {
        throw new Error('Paying to owner is not implemented.');
        return;
    }
}
exports.GroupAction = GroupAction;
