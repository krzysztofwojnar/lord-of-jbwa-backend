"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Game = void 0;
const Board_1 = require("./Board");
class Game {
    constructor(lobby) {
        this.losersPlayers = [];
        this.activePlayers = lobby.getPlayers();
        this.board = new Board_1.Board(lobby.getBoard());
    }
    getPlayers() {
        return this.activePlayers.concat(this.losersPlayers);
    }
    losePlayers(player) {
        const playerIndex = this.activePlayers.indexOf(player);
        if (0 <= this.activePlayers.indexOf(player)) {
            this.activePlayers = this.activePlayers.slice(0, playerIndex).concat(this.activePlayers.slice(playerIndex + 1));
            this.losersPlayers.push(player);
        }
    }
}
exports.Game = Game;
