"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupSubject = void 0;
class GroupSubject {
    constructor() {
        this.sameSubjectGroups = new Set();
    }
    addGroupWithSameSubject(group) {
        this.sameSubjectGroups.add(group);
        return group;
    }
    getGroupsWithSameSubject() {
        return new Set(this.sameSubjectGroups);
    }
}
exports.GroupSubject = GroupSubject;
