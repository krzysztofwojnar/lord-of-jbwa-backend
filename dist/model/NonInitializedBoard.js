"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NonInitializedBoard = void 0;
const fieldsService_1 = require("../fieldsService");
class NonInitializedBoard {
    constructor() {
        this.fields = fieldsService_1.FieldsService.fieldsService.getNonInitializedFields();
    }
    getFields() {
        return this.fields;
    }
}
exports.NonInitializedBoard = NonInitializedBoard;
