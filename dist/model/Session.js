"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Session = void 0;
const Game_1 = require("./Game");
const Lobby_1 = require("./Lobby");
class Session {
    constructor(id, owner) {
        this.lobby = new Lobby_1.Lobby(owner);
        this.id = id;
    }
    getLobby() {
        return this.lobby;
    }
    getOrStartGame() {
        if (this.game)
            return this.game;
        this.game = new Game_1.Game(this.lobby);
        return this.game;
    }
    getID() {
        return this.id;
    }
    ;
}
exports.Session = Session;
