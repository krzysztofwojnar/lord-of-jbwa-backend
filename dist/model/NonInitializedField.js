"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NonInitializedField = void 0;
class NonInitializedField {
    constructor(label, fullName, action, display, position) {
        this.label = label;
        this.fullName = fullName;
        this.action = action;
        this.display = display;
        this.position = position;
    }
    getLabel() { return this.label; }
    setLabel(newLabel) { this.label = newLabel; }
    getFullName() { return this.fullName; }
    setFullName(newFullName) { this.fullName = newFullName; }
    getPosition() { return this.position; }
    setPosition(newPosition) {
        if (newPosition >= 0) {
            this.position = newPosition;
        }
        else {
            throw new Error('New position is incorrect! ( ' + this.position + ' -> ' + newPosition + ')');
        }
    }
    getDisplay() { return this.display; }
    setDisplay(newDisplay) { this.display = newDisplay; }
    ;
    getAction() { return this.action; }
    setAction(newAction) { this.action = newAction; }
}
exports.NonInitializedField = NonInitializedField;
