"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Board = void 0;
const Field_1 = require("./Field");
class Board {
    constructor(board) {
        this.fields = board.getFields().map(nonInitializedField => new Field_1.Field(nonInitializedField));
    }
}
exports.Board = Board;
