"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Lobby = void 0;
const NonInitializedBoard_1 = require("./NonInitializedBoard");
class Lobby {
    constructor(host) {
        this.board = new NonInitializedBoard_1.NonInitializedBoard();
        this.host = host;
        this.players = [this.host];
    }
    getBoard() {
        return this.board;
    }
    getPlayers() {
        return this.players;
    }
}
exports.Lobby = Lobby;
