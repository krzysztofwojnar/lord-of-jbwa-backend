"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FieldsService = void 0;
const defaultGroupsNames_1 = require("./data/defaultGroupsNames");
const NonInitializedField_1 = require("./model/NonInitializedField");
const ClaimableFieldAction_1 = require("./model/FieldActions/ClaimableFieldAction");
const GroupSubject_1 = require("./model/GroupSubject");
const GroupAction_1 = require("./model/FieldActions/GroupAction");
const groupCostsAndPayments_1 = require("./data/groupCostsAndPayments");
const app_1 = require("./app");
class Chan extends ClaimableFieldAction_1.ClaimableFieldAction {
    constructor(cost) {
        super(cost);
        this.payments = [50, 100, 200, 400];
    }
    payToOwner(activePlayer) {
        const sameOwnerChans = Array.from(FieldsService.allSpecials.getGroupsWithSameSubject()).filter(field => field.getAction().getOwner() === this.owner).length;
        const payment = this.payments[sameOwnerChans - 1];
        activePlayer.debit(payment, this.getOwner());
    }
}
class SpecialAction extends ClaimableFieldAction_1.ClaimableFieldAction {
    constructor(cost) {
        super(cost);
    }
    payToOwner(activePlayer) {
        const roll = app_1.diceRolls();
        const multiplier = Array.from(FieldsService.allSpecials.getGroupsWithSameSubject()).filter(field => field.getAction().getOwner() === this.owner).length;
        const payment = multiplier * (roll.firstDice + roll.secondDice);
        activePlayer.debit(payment, this.getOwner());
    }
}
class FieldsService {
    constructor() {
        /*public static Chan = class extends ClaimableFieldAction {
          constructor(cost: number) {
            super(cost)
          }
          payments = [50, 100, 200, 400]
          payToOwner(activePlayer: Player) {
            const sameOwnerChans = Array.from(FieldsService.allSpecials.getGroupsWithSameSubject()).filter(field => field.getAction().getOwner() === this.owner).length;
            const payment = this.payments[sameOwnerChans-1];
            activePlayer.debit(payment, this.getOwner());
          }
        }*/
        this.chans = ['4chan', 'wykop', 'vichan', 'karachan']
            .map((name) => {
            const nonInitializedChan = new NonInitializedField_1.NonInitializedField(name, name, new Chan(400), {}, undefined);
            if (!FieldsService.allChans)
                FieldsService.allChans = new GroupSubject_1.GroupSubject();
            FieldsService.allChans.addGroupWithSameSubject(nonInitializedChan);
            return nonInitializedChan;
        });
        this.chanAction = {
            action: (field, activePlayer) => {
                throw new Error('Chan actions are not implemented!');
            }
        };
        this.start = new NonInitializedField_1.NonInitializedField('start', 'Start', {
            action: () => {
                return;
            }
        }, {});
        this.warn1 = new NonInitializedField_1.NonInitializedField('warn', 'Twój post nie spełnia standardów społeczności', {
            action: (activePlayer) => {
                activePlayer.updateBalance(-400);
            }
        }, {});
        this.aresztawka = new NonInitializedField_1.NonInitializedField('aresztawka', 'Weryfikacja', {
            action: () => {
                return;
            }
        }, {});
        this.hajsownicy = (() => {
            const field = new NonInitializedField_1.NonInitializedField('hajsownicy', 'Hajsownicy Gimpera', new SpecialAction(FieldsService.specialFieldCost), {});
            if (!FieldsService.allSpecials)
                FieldsService.allSpecials = new GroupSubject_1.GroupSubject();
            FieldsService.allSpecials.addGroupWithSameSubject(field);
            return field;
        })();
        this.shitpost = new NonInitializedField_1.NonInitializedField('shitpost', 'Shitpost', {
            action: () => {
            }
        }, { imgSrc: 'assets/shitpost.jpg' });
        this.bp = (() => {
            const field = new NonInitializedField_1.NonInitializedField('bp', 'Bękarty Putina', new SpecialAction(FieldsService.specialFieldCost), {});
            if (!FieldsService.allSpecials)
                FieldsService.allSpecials = new GroupSubject_1.GroupSubject();
            FieldsService.allSpecials.addGroupWithSameSubject(field);
            return field;
        })();
        this.ban = new NonInitializedField_1.NonInitializedField('ban', 'You are temporarily blocked', {
            action: (activePlayer) => {
                activePlayer.ban(3);
            }
        }, {});
        this.warn2 = new NonInitializedField_1.NonInitializedField('warn', 'Twój komentarz nie spełnia standardów społeczności', {
            action: (activePlayer) => {
                activePlayer.updateBalance(-200);
            }
        }, {});
    }
    static next() {
        let currentIndex = 0;
        return () => FieldsService.groups[currentIndex++];
    }
    static nextGroupCost() {
        let currentIndex = 0;
        const costs = groupCostsAndPayments_1.getCosts().reverse().flatMap(x => x);
        return () => costs[currentIndex++];
    }
    static chanceFactory(chanceType) {
        switch (chanceType) {
            case 'blue':
                return new NonInitializedField_1.NonInitializedField('cenzopapa', 'Cenzopapa', this.blueChance, { imgSrc: 'assets/jp2.png' });
            case 'red':
                return new NonInitializedField_1.NonInitializedField('testo', 'Wielki Testo', this.redChance, { imgSrc: 'assets/testo.jpg' });
        }
    }
    getNonInitializedFields() {
        const nextGroup = FieldsService.next();
        const blue = 'blue';
        const red = 'red';
        const allGroups = [
            this.start,
            nextGroup(),
            FieldsService.chanceFactory(blue),
            //Object.create(this.cenzopapa),
            nextGroup(),
            this.warn1,
            this.chans[0],
            nextGroup(),
            FieldsService.chanceFactory(red),
            nextGroup(),
            nextGroup(),
            this.aresztawka,
            nextGroup(),
            this.hajsownicy,
            nextGroup(),
            nextGroup(),
            this.chans[1],
            nextGroup(),
            FieldsService.chanceFactory(blue),
            nextGroup(),
            nextGroup(),
            this.shitpost,
            nextGroup(),
            FieldsService.chanceFactory(red),
            nextGroup(),
            nextGroup(),
            this.chans[2],
            nextGroup(),
            nextGroup(),
            this.bp,
            nextGroup(),
            this.ban,
            nextGroup(),
            nextGroup(),
            FieldsService.chanceFactory(blue),
            nextGroup(),
            this.chans[3],
            FieldsService.chanceFactory(red),
            nextGroup(),
            this.warn2,
            nextGroup(),
        ];
        allGroups.forEach((elem, index) => elem.setPosition(index));
        return allGroups;
    }
}
exports.FieldsService = FieldsService;
FieldsService.fieldsService = new FieldsService();
FieldsService.getDefaultGroups = (() => {
    const getNextGroupCost = FieldsService.nextGroupCost();
    return defaultGroupsNames_1.defaultGroupsNames.map((groupName, index) => {
        // console.log(getNextGroupCost());
        const displaySettings = getNextGroupCost();
        return new NonInitializedField_1.NonInitializedField(groupName.label, groupName.fullName, new GroupAction_1.GroupAction(displaySettings.cost, displaySettings.payments, displaySettings.moderatorsCost), { headerColor: displaySettings.header });
    }).reverse();
})();
FieldsService.groups = (() => {
    const getNextGroupCost = FieldsService.nextGroupCost();
    return defaultGroupsNames_1.defaultGroupsNames.map((groupName) => {
        const displaySettings = getNextGroupCost();
        return new NonInitializedField_1.NonInitializedField(groupName.label, groupName.fullName, new GroupAction_1.GroupAction(displaySettings.cost, displaySettings.payments, displaySettings.moderatorsCost), { headerColor: displaySettings.header }, undefined);
    }).reverse();
})();
FieldsService.allChans = new GroupSubject_1.GroupSubject();
FieldsService.blueChance = {
    action: (activePlayer) => {
        throw new Error('Blue Chance is not implemented!');
    }
};
FieldsService.redChance = {
    action: (activePlayer) => {
        throw new Error('Red Chance is not implemented!');
    }
};
FieldsService.specialFieldCost = 300;
FieldsService.allSpecials = new GroupSubject_1.GroupSubject();
