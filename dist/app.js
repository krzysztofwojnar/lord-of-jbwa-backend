"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.diceRolls = void 0;
const express_1 = __importDefault(require("express"));
const Session_1 = require("./model/Session");
const Player_1 = require("./model/Player");
const http = __importStar(require("http"));
const WebSocket = __importStar(require("ws"));
console.log('hello world!');
const app = express_1.default();
//simple http server
const server = http.createServer(app);
//WebSocket server instance
const webSocketServer = new WebSocket.Server({ server });
const games = [];
let counter = 0;
const singleDiceRoll = () => Math.floor((Math.random() * 6)) + 1;
exports.diceRolls = () => {
    return { firstDice: singleDiceRoll(), secondDice: singleDiceRoll() };
};
const aliveConnections = new Set();
webSocketServer.on('connection', (ws) => {
    aliveConnections.add(ws);
    ws.on('pong', () => {
        aliveConnections.add(ws);
        ws.send('You are alive;');
    });
    ws.on('message', (messageArrayBuffer) => {
        const message = messageArrayBuffer.toString();
        console.log(message);
        const NOT_IMPLEMENTED = 'not implemented yet!';
        if (message.match("make-an-offer" /* MAKE_AN_OFFER */ + '-.*')) {
            console.log('I see, you want to make a deal with user ' + message.substring("make-an-offer" /* MAKE_AN_OFFER */.length + 1));
            ws.send('I see, you want to make a deal with user ' + message.substring("make-an-offer" /* MAKE_AN_OFFER */.length + 1));
        }
        else
            switch (message) {
                case "dice-roll" /* DICE_ROLL */:
                    const roll = JSON.stringify({ requestType: message, results: exports.diceRolls(), drawNumber: ++counter });
                    console.log(roll);
                    ws.send(roll);
                    break;
                case "start-game" /* START_GAME */:
                    const startedGame = new Session_1.Session(0, new Player_1.Player('BlaskZKibla'));
                    const gamePosition = games.push(startedGame);
                    console.log(JSON.stringify({ requestType: message, ...startedGame.getLobby().getBoard() }));
                    ws.send(JSON.stringify({ requestType: message, ...startedGame.getLobby().getBoard() }));
                    break;
                case "update-players" /* UPDATE_PLAYERS */:
                    ws.send(message + ' is ' + NOT_IMPLEMENTED);
                    throw new Error(NOT_IMPLEMENTED);
                    break;
                case "update-board" /* UPDATE_BOARD */:
                    ws.send(message + ' is ' + NOT_IMPLEMENTED);
                    throw new Error(NOT_IMPLEMENTED);
                    break;
                default:
                    ws.send(message + ' not recognized');
                //ws.close(404);
            }
    });
});
/*
setInterval(() => {

    console.log(webSocketServer.clients.size);
    // @ts-ignore
    webSocketServer.clients.forEach((ws: WebSocket) => {
        // console.log(aliveConnections.has(ws));
        if (!aliveConnections.has(ws)) ws.terminate();
        ws.ping(null, false);
    } );
    aliveConnections.clear();
}, 10000);
*/
//start our server
server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on port ${server.address()} :)`);
    console.log(server.address());
});
/*
const API_URL = '*';
const options:cors.CorsOptions = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: API_URL,
    preflightContinue: false
  };
  
app.use(cors(options));

app.get('/dice-roll', (req: Request, res: Response, next: NextFunction) => {
    res.send(
        function(){
            let roll = JSON.stringify({results: diceRolls(), drawNumber: ++counter});
            console.log(roll);
            return roll;
    }());
});
app.get('/start-game', (req: Request, res: Response, next: NextFunction) => {
    res.setHeader('content-type', 'application/json');
    console.log(req)
    res.send((()=>{
        const startedGame = new Session(0, new Player('BlaskZKibla'));
        const gamePosition = games.push(startedGame)
        return JSON.stringify(startedGame.getLobby().getBoard());
    })())
})
app.get('/update-players', (req: Request, res: Response, next: NextFunction) => {
    res.setHeader('content-type', 'application/json');
    console.log(req);
    console.log('elo');
    res.send((()=>{
        const startedGame = games[games.length-1];
        const positions = startedGame.getLobby().getPlayers().map(
            player => ({nickname: player.getNickname(), position: player.getPosition()})
        )
        return JSON.stringify(positions);
    })())
})

app.listen(5000, () => { console.log('Server running') });

 */
