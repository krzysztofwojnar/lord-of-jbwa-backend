"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultGroupsNames = void 0;
exports.defaultGroupsNames = [
    { label: 'Ciekawostkawka', fullName: 'Ciekawostkawka' },
    { label: 'Szkieletorawka', fullName: 'Szkieletor Posting - Sekcja Szkieletorów' },
    { label: 'rozkminawka', fullName: 'rozkminawka - sekcja grubych rozkmin', },
    { label: 'Słodziawka', fullName: 'Słodziawka - sekcja cute images', },
    { label: 'Perfekcjonizmawka', fullName: 'Perfekcjonizmawka' },
    { label: 'Elekcjawka', fullName: 'Jak będzie na e-lekcjach?' },
    { label: 'Uwagawka', fullName: 'Jak będzie u dyrektora? – sekcja uwag szkolnych' },
    { label: 'Post- nostalgawka', fullName: 'Jak będzie w 2030?' },
    { label: 'Słodkawka', fullName: 'Słodkawka - sekcja przyjemnych obrazków 😺❤' },
    { label: 'Lumpawka', fullName: 'Lumpawka 👕 – sekcja second handów' },
    { label: 'AAAAAAA', fullName: 'Jak będzie w AAAAAAAAAAAAAAAAAAAAA? – sekcja AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA' },
    { label: 'Składniawka', fullName: 'Składniawka – sekcja alternatywnego języka polskiego' },
    { label: 'Sypialniawka', fullName: 'Sypialniawka – miesiąc widoczności osób aseksualnych 🏳️‍🌈⚡' },
    { label: 'Sekcja past', fullName: 'Sekcja past' },
    { label: 'Hanuszkawka', fullName: 'Jak będzie w hanuszkach?' },
    { label: 'Nagłówkawka', fullName: 'Jak będzie w nagłówkach? - [ZOBACZ JAK]' },
    { label: 'Nostalgawka', fullName: 'Jak było w 2011? -- Sekcja Nostalgiczna' },
    { label: 'Ankietawka', fullName: 'Ankietawka - Sekcja szybkich strzałów' },
    { label: 'Tuptawka', fullName: 'Tuptawka' },
    { label: 'Galeriawka', fullName: 'Jak będzie w galerii? – sekcja kradzionych memów' },
    { label: 'Pytawka', fullName: 'Pytawka – sekcja pytań i odpowiedzi' },
    { label: 'Skrybawka', fullName: 'Skrybawka - Mamo, Kleopatra kazała pałac na jutro' }
];
/*
Jak będzie w nagłówkach? - [ZOBACZ JAK]	37574
Błogosławka – Sekcja blessed images	37059
Pytawka – sekcja pytań i odpowiedzi	35322
Jak być nie powinno? – sekcja ostrzegania przed błędami	34799
Sekcja Filmowa
Jak będzie na mapie? – sekcja geograficzno-kartograficzna
Jak będzie u dyrektora? - Sekcja uwag szkolnych
Jak będzie w szkieletorze? - Sekcja szkieletorów

Sekcja past	74996
Ciekawostkawka	72642
Jak będzie w 2030?	71845
Perfekcjonizmawka	67262
Jak było w 2011? -- Sekcja Nostalgiczna	61351
Jak będzie w hanuszkach?	57865
Sekcja beków z zaskoczenia	52368
Jak będzie w AAAAAAAAAAAAAAAAAAAAA? – sekcja AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA	51079
Sypialniawka - sekcja edukacji seksualnej	46812
jak będzie w translatorze - sekcja tłumaczonych memów	46332
Klątawka – Sekcja cursed images	44764
Jak będzie w akapie? – sekcja memów	42162
Składniawka – sekcja alternatywnego języka polskiego	39135
*/
