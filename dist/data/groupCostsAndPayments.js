"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCosts = void 0;
exports.getCosts = function () {
    const y = 'yellow';
    const r = 'red';
    const bl = 'blue';
    const o = 'orange';
    const g = 'green';
    const v = 'violet';
    const br = 'brown';
    const black = 'black';
    const moderatorsCosts = [100, 200, 300, 400];
    return [
        [
            { cost: 120, payments: [10, 40, 120, 360, 640, 900], moderatorsCost: moderatorsCosts[0], header: y },
            { cost: 120, payments: [10, 40, 120, 360, 640, 900], moderatorsCost: moderatorsCosts[0], header: y },
        ],
        [
            { cost: 200, payments: [15, 60, 180, 540, 800, 1100], moderatorsCost: moderatorsCosts[0], header: r },
            { cost: 200, payments: [15, 60, 180, 540, 800, 1100], moderatorsCost: moderatorsCosts[0], header: r },
            { cost: 240, payments: [20, 80, 200, 600, 900, 1200], moderatorsCost: moderatorsCosts[0], header: r },
        ],
        [
            { cost: 280, payments: [20, 100, 300, 900, 1250, 1500], moderatorsCost: moderatorsCosts[1], header: bl },
            { cost: 280, payments: [20, 100, 300, 900, 1250, 1500], moderatorsCost: moderatorsCosts[1], header: bl },
            { cost: 320, payments: [25, 120, 360, 1000, 1400, 1800], moderatorsCost: moderatorsCosts[1], header: bl },
        ],
        [
            { cost: 360, payments: [30, 140, 400, 1100, 1500, 1900], moderatorsCost: moderatorsCosts[1], header: o },
            { cost: 360, payments: [30, 140, 400, 1100, 1500, 1900], moderatorsCost: moderatorsCosts[1], header: o },
            { cost: 400, payments: [35, 160, 440, 1200, 1600, 2000], moderatorsCost: moderatorsCosts[1], header: o },
        ],
        [
            { cost: 440, payments: [35, 180, 500, 1400, 1750, 2100], moderatorsCost: moderatorsCosts[2], header: g },
            { cost: 440, payments: [35, 180, 500, 1400, 1750, 2100], moderatorsCost: moderatorsCosts[2], header: g },
            { cost: 480, payments: [40, 200, 600, 1500, 1850, 2200], moderatorsCost: moderatorsCosts[2], header: g },
        ],
        [
            { cost: 520, payments: [45, 220, 660, 1600, 1950, 2300], moderatorsCost: moderatorsCosts[2], header: v },
            { cost: 520, payments: [45, 220, 660, 1600, 1950, 2300], moderatorsCost: moderatorsCosts[2], header: v },
            { cost: 560, payments: [50, 240, 720, 1700, 2050, 2400], moderatorsCost: moderatorsCosts[2], header: v },
        ],
        [
            { cost: 600, payments: [55, 260, 780, 1900, 2200, 2550], moderatorsCost: moderatorsCosts[3], header: br },
            { cost: 600, payments: [55, 260, 780, 1900, 2200, 2550], moderatorsCost: moderatorsCosts[3], header: br },
            { cost: 640, payments: [60, 300, 900, 2000, 2400, 2800], moderatorsCost: moderatorsCosts[3], header: br },
        ],
        [
            { cost: 700, payments: [70, 350, 1000, 2200, 2600, 3000], moderatorsCost: moderatorsCosts[3], header: black },
            { cost: 800, payments: [100, 400, 1200, 2800, 3400, 4000], moderatorsCost: moderatorsCosts[3], header: black }
        ]
    ];
};
